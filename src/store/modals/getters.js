const getters = {
  processModals: (state) => state.processModals,
  hasBlockingModals: (state) => !!state.processModals.length,
};

export default getters;