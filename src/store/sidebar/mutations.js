const mutations = {
  setFilterTags: (state, tags) => state.filter.tags = tags,
  addFilterTag: (state, tag) => state.filter.tags = [...new Set([...state.filter.tags, tag])],
  removeFilterTag: (state, tag) => state.filter.tags = state.filter.tags.filter(t => t !== tag),
  setFilterQuery: (state, query) => state.filter.query = query || '',
};

export default mutations;