const getters = {
  position: (state)  => state.position,
  visibility: (state) => state.visible,
  options: (state, getters, rootState, rootGetters) => {
    const {
      'editor/selectedModuleIndex': selectedModuleIndex,
      'editor/getAllowedSections': getAllowedSections,
      'editor/getAllowedCards': getAllowedCards,
    } = rootGetters;

    return getAllowedSections(selectedModuleIndex).concat(getAllowedCards(selectedModuleIndex));
  },
  selectedOption: (state) => state.selectedOption,
};

export default getters;