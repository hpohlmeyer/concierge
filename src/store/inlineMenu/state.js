const state = {
  position: { x: 0, y: 0 },
  visible: false,
  selectedOptions: [],
};

export default state;