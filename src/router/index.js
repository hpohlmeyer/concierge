import Vue from 'vue';
import VueRouter from 'vue-router';

// Pages
import WelcomePage from '../components/pages/WelcomePage.vue';
import ProjectPage from '../components/pages/ProjectPage.vue';

// Views
import PostView from '../components/views/PostView.vue';
import NoPostView from '../components/views/NoPostView.vue';

Vue.use(VueRouter);

export default new VueRouter({
  routes: [
    {
      path: '/',
      name: 'welcome',
      component: WelcomePage,
    },
    {
      path: '/post',
      component: ProjectPage,
      children: [
        {
          path: '',
          component: NoPostView,
        },
        {
          path: ':post_id',
          component: PostView,
        },
      ],
    },
  ],
});
