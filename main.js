// Basic init
const electron = require('electron');
const { app, BrowserWindow, Menu } = electron;
const baseMenu = require('./src/menus/BaseMenu');

app.commandLine.appendSwitch('allow-file-access-from-files');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

const windowUrl = process.env.NODE_ENV === 'development'
  ? 'http://localhost:8080/'
  : `file://${__dirname}/index.html`;

function createWindow() {
  // Create the browser window
  mainWindow = new BrowserWindow({
    width: 1200,
    height: 900,
    minWidth: 1125,
    minHeight: 590,
    titleBarStyle: 'hidden',

    // this is neccessary due to electron not supporting
    // the allow-file-access-from-files chrome flag.
    webPreferences: {
      webSecurity: false,
    },
  });

  // The offset of electron dialogs.
  // Should mirror the title bar height.
  mainWindow.setSheetOffset(0);

  Menu.setApplicationMenu(baseMenu);

  // load the index.html of the app
  mainWindow.loadURL(windowUrl);

  // Open dev-tools by default in dev mode
  // if(process.env.NODE_ENV === 'development') {
  //   mainWindow.webContents.openDevTools();
  // }

  mainWindow.on('closed', () => {
    // Dereference all open windows to alow garbage collection
    mainWindow = null;
  });
}

function initDevtools() {
  if (process.env.NODE_ENV === 'production') {
    require('vue-devtools').uninstall();
  } else {
    require('vue-devtools').install();
  }
}

function setMenuItemState(menuItemId, property, value) {
  if (property !== 'enabled' && property !== 'visible') { return; }
  const menuItem = baseMenu.getMenuItemById(menuItemId);
  if (!menuItem) { return; }
  menuItem[property] = value;
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', initDevtools);
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

electron.ipcMain.on('disable-menu-item', (_, menuItemId) => {
  setMenuItemState(menuItemId, 'enabled', false);
});

electron.ipcMain.on('enable-menu-item', (_, menuItemId) => {
  setMenuItemState(menuItemId, 'enabled', true);
});