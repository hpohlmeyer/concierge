const getters = {
  modules: (state) => state.modules,

  getModule: (state) => (moduleIndex) => state.modules[moduleIndex],

  getOptions: (state, getters, rootState, rootGetters) => (moduleIndex) => {
    return {
      mobiledoc: rootGetters['posts/getSelectedPostModuleByIndex'](moduleIndex),
      atoms: rootGetters['project/availableAtoms'],
      cards: rootGetters['project/availableCards'],
      markup: rootGetters['project/availableMarkup'],
      sections: rootGetters['project/availableSections'],
      placeholder: '',
      autofocus: !!moduleIndex,
      spellCheck: true,
      enableEditing: true,
    };
  },
  
  getActiveMarkupTags: (state, { getModule }) => (moduleIndex) => {
    const module = getModule(moduleIndex);
    return module.activeMarkupTags;
  },
  
  getActiveSectionTags: (state, { getModule }) => (moduleIndex) => {
    const module = getModule(moduleIndex);
    return module.activeSectionTags;
  },

  apiAvailable: (state, { getModule }) => (moduleIndex) => {
    const module = getModule(moduleIndex);
    return !!module.api;
  },

  getModuleTemplate: (state, getters, rootState, rootGetters) => (moduleIndex) => {
    const { 'posts/selectedPostTemplate': selectedTemplate } = rootGetters;

    if (!selectedTemplate) { return; }
    
    const moduleTemplate = selectedTemplate.modules && selectedTemplate.modules[moduleIndex];
    if (!moduleTemplate) { return; }

    return moduleTemplate;
  },

  getAllowedSections: (state, { getModuleTemplate }) => (moduleIndex) => {
    const moduleTemplate = getModuleTemplate(moduleIndex);
    return moduleTemplate && moduleTemplate.allowedSections || [];
  },
  
  getAllowedMarkup: (state, { getModuleTemplate }) => (moduleIndex) => {
    const moduleTemplate = getModuleTemplate(moduleIndex);
    return moduleTemplate && moduleTemplate.allowedMarkup || [];
  },
  
  getAllowedAtoms: (state, { getModuleTemplate }) => (moduleIndex) => {
    const moduleTemplate = getModuleTemplate(moduleIndex);
    return moduleTemplate && moduleTemplate.allowedAtoms || [];
  },
  
  getAllowedCards: (state, { getModuleTemplate }) => (moduleIndex) => {
    const moduleTemplate = getModuleTemplate(moduleIndex);
    return moduleTemplate && moduleTemplate.allowedCards || [];
  },

  // These have to return a function, otherwise the value will get cached and wrong values appear.
  editorHasSelection: (state, { modules }) => () => modules.some(module => module.api && module.api.hasSelection()),
  editorHasCursor: (state, { modules }) => () => modules.some(module => module.api && module.api.hasCursor()),

  selectedModuleIndex: (state) => 0,
};

export default getters;