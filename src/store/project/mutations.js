import cleanConfig from '../../utils/cleanConfig';

const mutations = {
  setConfig: (state, config) => state.config = cleanConfig(config),
  clearConfig: (state) => state.config = null,
  setProjectPath: (state, projectPath) => state.projectPath = projectPath,
};

export default mutations;