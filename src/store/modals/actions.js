import { generateUniqueId } from '../../utils';

const actions = {
  /**
   * Processes a number of tasks while setting their status.
   * The function modifies the tasks array and adds a `status` 
   * property to the task, which can be either `PENDING`, `RUNNING`,
   * `FINISHED` or `CANCELED`.
   * 
   * @param {Object[]}                                  tasks
   * @param {Function|Promise}                          tasks[].func - The task function
   * @param {[String=Please wait a moment, while we …]} tasks[].label - What does the task do?
   *                                                                    Complete the sentence, "Wait until we …"
   */
  async runTasksWithModal({ commit }, { intro, tasks } = { intro: 'Please wait a moment, while we …' }) {
    if (!tasks) { throw new Error('No tasks have been defined for the process modal.'); }
    const modalId = generateUniqueId();
    commit('_processModalCreate', { modalId, intro, tasks });

    // Start running the tasks sequentially.
    for (const [taskIndex, task] of tasks.entries()) {
      commit('_processModalChangeStatus', { modalId, taskIndex, status: 'RUNNING' });
      try {
        await task.func();
        commit('_processModalChangeStatus', { modalId, taskIndex, status: 'FINISHED' });
      } catch (err) {
        commit('_processModalChangeStatus', { modalId, taskIndex, status: 'CANCELED' });
        // TODO: Show error log to the user
        throw err;
      } finally {
        commit('_processModalRemove', { modalId });
      }
    }
  },
};

export default actions;