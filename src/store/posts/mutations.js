import Vue from 'vue';

const mutations = {
  addPost: (state, post) => {
    Vue.set(state.posts, post.id, post);
    state.postOrder.unshift(post.id);
  },

  selectPost: (state, id) => state.selectedPostId = id,

  updatePost: (state, [id, post]) => Vue.set(state.posts, id, post),
  
  updatePosts: (state, posts) => state.posts = posts,
  
  updatePostMeta: (state, [postId, metaKey, metaVal]) => Vue.set(state.posts[postId].metadata, metaKey, metaVal),

  updatePostModule: (state, [postId, moduleId, content]) => Vue.set(state.posts[postId].modules, moduleId, content),
  
  updatePostOrder: (state, postsArray) => state.postOrder = postsArray,
  
  deletePost: (state, id) => { 
    Vue.delete(state.posts, id);
    Vue.delete(state.postOrder, state.postOrder.indexOf(id));
    if (state.selectedPostId === id) { state.selectedPostId = undefined; }
  },

  unloadPosts: (state) => {
    state.selectedPostId = null;
    state.posts = {};
    state.postOrder = [];
  },

  setSearchQuery: (state, query) => {
    state.searchQuery = query;
  },
};

export default mutations;