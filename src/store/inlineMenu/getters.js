const getters = {
  position: (state)  => state.position,
  visibility: (state) => state.visible,
  options: (state, getters, rootState, rootGetters) => {
    const {
      'editor/selectedModuleIndex': selectedModuleIndex,
      'editor/getAllowedMarkup': getAllowedMarkup,
      'editor/getAllowedAtoms': getAllowedAtoms,
    } = rootGetters;

    return getAllowedMarkup(selectedModuleIndex).concat(getAllowedAtoms(selectedModuleIndex));
  },
  selectedOptions: (state) => state.selectedOptions,
};

export default getters;