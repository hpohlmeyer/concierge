const state = {
  position: { x: 0, y: 0 },
  visible: false,
  selectedOption: null,
};

export default state;