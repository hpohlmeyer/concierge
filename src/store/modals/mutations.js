const mutations = {
  _processModalCreate: (state, { modalId, intro, tasks}) => {
    const modal = {
      id: modalId,
      intro,
      tasks: tasks.map(task => ({ ...task, status: 'PENDING' })),
    };
    state.processModals = [...state.processModals, modal];
  },

  _processModalChangeStatus: (state, { modalId, taskIndex, status }) => {
    state.processModals = state.processModals.map(modal => {
      if (modal.id === modalId) { modal.tasks[taskIndex].status = status; }
      return modal;
    });
  },

  _processModalRemove: (state, { modalId }) => {
    state.processModals = state.processModals.filter(modal => modal.id !== modalId);
  },
};

export default mutations;