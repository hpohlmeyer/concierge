const BLOCK_MENU_OFFSET = -100;

const actions = {
  async updatePosition({ commit, rootGetters }) {
    const { 'editor/editorHasCursor': editorHasCursor } = rootGetters;
    if (getSelection().rangeCount < 1 || !editorHasCursor()) { return; }
    const currentElement = getSelection().getRangeAt(0).startContainer.parentElement.closest('.content-module__content > *')
                        || getSelection().getRangeAt(0).startContainer;
    const clientRect = currentElement.getBoundingClientRect();
    const x = clientRect.left - BLOCK_MENU_OFFSET;
    const y = clientRect.top + clientRect.height / 2;
    commit('setPosition', { x, y });
  },

  async updateOptions({ commit }, options) {
    commit('setOptions', options);
  },

  async updateSelectedOption({ commit }, option) {
    commit('setSelectedOption', option);
  },

  async setVisibility({ commit }, visibility) {
    commit('setVisibility', visibility);
  },
};

export default actions;