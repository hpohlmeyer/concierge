import * as rollup from 'rollup';
import fse from 'fs-extra';
import router from '../../router';
import { resolve } from 'path';
import { readdir } from 'fs-extra';
import { spawn } from 'child_process';
import { ipcRenderer, shell } from 'electron'; 
import {
  showOpenDialogAsync,
  showSaveDialogAsync,
  getObjectSubset,
  recursiveUploadDir,
} from '../../utils';

const actions = {
  async openProject({ getters, commit, dispatch }, projectPath) {
    dispatch('closeProject');

    if (!projectPath) {
      const dialogSettings = { properties: ['openDirectory'] };
      projectPath = await showOpenDialogAsync(dialogSettings);
      if (projectPath === undefined) { return; }
      projectPath = projectPath[0];
    }

    // Set project path
    commit('setProjectPath', projectPath);

    // Load config from disk
    await dispatch('_loadConfigFile', getters.configPath);

    // Load posts from disk
    let files = await readdir(getters.postsDir);
    files = files
      .filter(file => /^post(\d|[a-z])+\.json$/.test(file))
      .map(file => resolve(getters.postsDir, file));
            
    await Promise.all(files.map(async file => dispatch('posts/loadPostFromDisk', file, { root: true })));

    // TODO:
    // Handle errors when reading all the files.
    // If there are any, reset the project and show an error.

    ipcRenderer.send('enable-menu-item', 'close-project');
    ipcRenderer.send('enable-menu-item', 'export-site');
    ipcRenderer.send('enable-menu-item', 'preview-site');
    if (getters.ftpSettings && getters.ftpSettings.host) {
      ipcRenderer.send('enable-menu-item', 'deploy-site');
    }

    router.push('/post');
  },

  closeProject({ commit }) {
    ipcRenderer.send('disable-menu-item', 'close-project');
    ipcRenderer.send('disable-menu-item', 'export-site');
    ipcRenderer.send('disable-menu-item', 'preview-site');
    ipcRenderer.send('disable-menu-item', 'deploy-site');
    commit('clearConfig');
    commit('setProjectPath', null);
    commit('posts/unloadPosts', null, { root: true });
    commit('blockMenu/setVisibility', false, { root: true });
    router.push('/');
  },

  async _loadConfigFile({ commit }, configPath) {
    // TODO: Replace this part with dynamic imports (`import()`),
    // as soon as possible!
    // The feature is available in Chrome 63, change it as soons as
    // Chrome 63+ hits electron.
    //
    // const config = import(configPath);

    // Rollup config file modules.
    const bundle = await rollup.rollup({ input: configPath });
    const { code } = await bundle.generate({
      format: 'iife',
      name: 'conciergeProject',
    });
    
    // Parse the module string to get usable js code.
    const config = eval(`(function () {${code} return conciergeProject; })()`);

    if (config) { commit('setConfig', config); }
  },

  buildProject({ getters }) {
    // Ensure a project has been opened
    if (!getters.projectPath) { return; }

    // Set environment variables for the static site generator
    const env = {
      CONCIERGE_CONFIG_PATH: getters.configPath,
      CONCIERGE_CONTENT_DIR: getters.contentDir,
      CONCIERGE_BUILD_DIR: getters.buildDir,
    };

    // Spawn method: returns data while processing.
    // Run the shell comannd
    return new Promise((resolve, reject) => {
      const shell = spawn(getters.buildCommand, [], { shell: true, cwd: getters.generatorDir, env });

      // Get text chunks instead of buffers
      shell.stdout.setEncoding('utf8');
      shell.stderr.setEncoding('utf8');

      // Listen to output
      // TODO: Write to log
      shell.stdout.on('data', data => { console.log('stdout', data); });
      shell.stderr.on('data', data => { console.error('stderr', data); });

      // Process finished event
      shell.on('close', code => {
        if (code === 0) {
          new Notification('Build successful', {
            body: 'The build has run successfully.',
            silent: true,
          });
          resolve();
        }
        reject(code);
      });
    });
  },

  async previewSite({ getters, dispatch }, build = true) {
    if (build) {
      const tasks = [{ label: '… build the site', func: () => dispatch('buildProject') }];
      await dispatch('modals/runTasksWithModal', { tasks }, { root: true });
    }

    const previewPath = `file://${getters.buildDir}/index.html`;
    const success = shell.openExternal(previewPath);
    if (!success) { throw new Error(`Sorry, we can not show you a preview of ${previewPath}. There seems to be no index file.`); }
  },

  async exportSite({ getters, dispatch }, build = true) {
    if (build) {
      const tasks = [{ label: '… build the site', func: () => dispatch('buildProject') }];
      await dispatch('modals/runTasksWithModal', { tasks }, { root: true });
    }

    const exportLocation = await showSaveDialogAsync();
    if (!exportLocation) { return; }

    await fse.copy(getters.buildDir, exportLocation);
  },

  async deploySite({ dispatch }, build = true) {
    const tasks = [];
    if (build) { tasks.push({ label: '… build the site', func: () => dispatch('buildProject') }); }
    tasks.push({ label: '… deploy it to the server', func: () => dispatch('_uploadSiteToServer') });

    await dispatch('modals/runTasksWithModal', { tasks }, { root: true });
  },

  async _uploadSiteToServer({ getters }, clean = true) {
    const settings = getters.ftpSettings;
    const config = getObjectSubset(getters.ftpSettings, ['host', 'port', 'user', 'password']);
    const ftp = (settings.protocol === 'ftp' && new (require('promise-ftp'))())
             || (settings.protocol === 'ftps' && new (require('promise-sftp'))());

    if (!ftp) { throw new Error(`"${settings.protocol}" is not a valid protocol. Use "ftp" or "sftp".`); }

    await ftp.connect(config);

    if (clean) {
      // Can be problematic at root level.
      // I am not sure how to handle this. Maybe only delete non-dot-files?
      // But that could cause problems with htacess and so on …

      // Ensure that the project folder exists
      await ftp.mkdir(getters.ftpSettings.remotePath, true);

      // Delete the folder and all its content
      // TODO: Test if recursive deletion also works with the sftp module.
      await ftp.rmdir(getters.ftpSettings.remotePath, true);
    }
  
    await recursiveUploadDir(ftp, getters.buildDir, getters.ftpSettings.remotePath);
  },
};

export default actions;