module.exports = {
  "env": {
    "browser": true,
    "es6": true,
    "node": true
  },
  "extends": [
    "eslint:recommended",
    "plugin:vue/recommended"
  ],
  "parserOptions": {
    "sourceType": "module",
    "ecmaVersion": 2017
  },
  "rules": {
    "linebreak-style": ["error", "unix"],
    "quotes": ["error", "single"],
    "semi": ["error", "always" ],
    "no-console": ["warn", { "allow": ["warn", "error"] }],
    "curly": "error",
    "dot-notation": "error",
    "eqeqeq": ["error", "always"],
    "no-extra-bind": "error",
    "no-floating-decimal": "error",
    "no-useless-call": "error",
    "no-useless-return": "error",
    "eqeqeq": ["error", "always"],
    "comma-dangle": ["error", "always-multiline"],
    "no-extend-native": "error",
    "no-useless-return": "error",
    "brace-style": ["error", "1tbs", { "allowSingleLine": true }],
    "array-bracket-spacing": ["error", "never"],
    "block-spacing": ["error", "always"],

    // vue rules
    "vue/v-bind-style": "error",
    "vue/v-on-style": "error",
    "vue/html-quotes": "error",
    "vue/name-property-casing": "error",
    "vue/mustache-interpolation-spacing": "error",
    "vue/no-shared-component-data": "error",
    "vue/html-self-closing": ["error", {
      "html": {
        "void": "always",
        "normal": "always",
        "component": "always",
      },
      "svg": "always",
      "math": "always",
    }],

    // Disabled, because of errors in vue-eslint
    "vue/max-attributes-per-line": "off",
    "vue/order-in-components": "off",
    // "vue/max-attributes-per-line": ["error", { singleLine: 10, multiline: { max: 1, allowSingleLine: false } }],
  }
};
