const actions = {
  updatePosition({ commit, rootGetters }) {
    const { 'editor/editorHasCursor': editorHasCursor } = rootGetters;
    if (getSelection().rangeCount < 1 || !editorHasCursor()) { return; }
    const clientRects = getSelection().getRangeAt(0).getClientRects();
    const selection = clientRects[clientRects.length - 1];
    const x = selection.left + selection.width / 2;
    const y = selection.bottom;
    commit('setPosition', { x, y });
  },
  
  updateSelectedOptions({ commit }, option) {
    commit('setSelectedOption', option);
  },
  
  setVisibility({ commit }, visibility) {
    commit('setVisibility', visibility);
  },
};

export default actions;