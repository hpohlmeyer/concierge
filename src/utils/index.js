import { EMPTY_MOBILEDOC } from './mobiledoc-templates';
import { remote } from 'electron';
import { join } from 'path';
import fse from 'fs-extra';
import { spawn } from 'child_process';

const { dialog } = remote;

/**
 * Clones a git repository.
 * 
 * @param {String}  repo The link to the git repository.
 * @param {String}  targetPath The target location of the cloned repository.
 * @param {Object}  [opts] 
 * @param {String}  [opts.git='git'] The location of the git executable.
 * @param {Boolean} [opts.shallow] When `true`, clone with depth 1.
 */
export function gitClone(repo, targetPath, opts = { git: 'git' }) {
  return new Promise((resolve, reject) => {

    const args = opts.shallow
                ? ['clone', '--', repo, targetPath]
                : ['clone', '--depth', '1', '--', repo, targetPath];

    const process = spawn(opts.git, args);

    process.on('close', (status) => {
      if (status !== 0) { return reject(new Error(`"git clone" failed with status code ${status}.`)); }
      resolve();
    });
  });
}

/**
 * Reads all files of a directory recursively.
 * 
 * @param   {String}   dir - The path of the directory, that should be read.
 * @param   {String[]} [allFiles=[]] - The files, that have already been found.
 * @returns {String[]} A list of all files in `dir` and its subdirs.
 */
export async function recursiveReaddir(dir, allFiles = []) {
  const files = (await fse.readdir(dir)).map(f => join(dir, f));
  allFiles.push(...files);
  await Promise.all(files.map(async f => {
    return (await fse.stat(f)).isDirectory() && recursiveReaddir(f, allFiles);
  }));
  return allFiles;
}

/**
 * Recursively uploads a folder and all its contents to the server.
 * 
 * @param {PromiseFtp} ftpClient - The connected PromiseFtp client.
 * @param {String} localDir  - The absolute path to the directory on the local machine.
 * @param {String} remoteDir - The absolute path to the directory on the server.
 */
export async function recursiveUploadDir(ftpClient, localDir, remoteDir) {
  // Ensure the folder exists on remote
  await ftpClient.mkdir(remoteDir, true);

  // Get all files in the current dir
  const files = (await fse.readdir(localDir))
    .map(f => ({ localPath: join(localDir, f), remotePath: join(remoteDir, f) }));

  // Upload files and go through subdirectories recursively
  await Promise.all(files.map(async ({ localPath, remotePath }) => {
    const stats = await fse.stat(localPath);
    if (stats.isFile()) { await ftpClient.put(localPath, remotePath); }
    if (stats.isDirectory()) {
      return recursiveUploadDir(ftpClient, localPath, remotePath);
    }
  }));
}

/**
 * Promise wrapper for electrons showOpenDialog, which opens a native file dialog.
 * 
 * Notes: The first optional browser argument is not
 *        supported! Also this should be replaced as soon
 *        as electron 2.0 hits.
 *        https://github.com/electron/electron/issues/9882
 * 
 * @export
 * @param   {[BrowserWindow]} browserWindow - The browser window for the dialog
 * @param   {Object}          options - See https://github.com/electron/electron/
 *                                          blob/master/docs/api/dialog.md
 *                                          #dialogshowopendialogbrowserwindow-options-callback
 * @returns {Promise}
 */
export function showOpenDialogAsync(...args) {
  return new Promise((resolve) => {
      dialog.showOpenDialog(...args, (result) => {
        resolve(result);
      });
  });
}

/**
 * Promise wrapper for electrons showOpenDialog, which opens a native file dialog.
 * 
 * Notes: The first optional browser argument is not
 *        supported! Also this should be replaced as soon
 *        as electron 2.0 hits.
 *        https://github.com/electron/electron/issues/9882
 * 
 * @export
 * @param   {[BrowserWindow]} browserWindow - The browser window for the dialog
 * @param   {Object}          options - See https://github.com/electron/electron/
 *                                          blob/master/docs/api/dialog.md
 *                                          #dialogshowsavedialogbrowserwindow-options-callback
 * @returns {Promise}
 */
export function showSaveDialogAsync(...args) {
  return new Promise((resolve) => {
    dialog.showSaveDialog(...args, (result) => {
      resolve(result);
    });
  });
}

/**
 * A date reviver for `JSON.parse()`. It parses date strings into JS dates.
 * 
 * @param {*} key 
 * @param {*} val 
 */
export function jsonDateReviver(key, val) {
  const regex = /^\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d.\d\d\dZ$/;
  return typeof val === 'string' && regex.test(val) ? new Date(val) : val;
}

/**
 * Creates an empty post object, which should be used to initialize posts.
 * 
 * @param {String} template - The name of the template, that should be used.
 */
export function createEmptyPost(template = 'default') {
  return {
    id: generateUniqueId(),
    metadata: {
      template,
      created: new Date(),
      tags: [],
    },
    modules: [
      EMPTY_MOBILEDOC,
    ],
  };
}

/**
 * Generate a unique id for posts.
 * 
 * Uses Date.now to get a unique timestamp and adds a random number
 * to avoid problems, when multiple IDs are generated in rapid succession.
 */
export function generateUniqueId() {
  return Date.now().toString(16) + crypto.getRandomValues(new Uint32Array(1))[0];
}

/**
 * Shortens text typographically and only on word boundaries.
 * 
 * @param {String}  text - The text to shorten
 * @param {Number}  length - The max length of the shortened text
 * @param {Boolean} [replaceNewLines=false] - Replace new lines with spaces?
 */
export function shortenText(text, length, replaceNewLines = false) {
  let optimizedText = text
    .slice(0, length + 2)
    .trim();
  
  if (replaceNewLines) {
    optimizedText = optimizedText.replace('\n', ' ');
  }
  
  // Split into words
  const words = optimizedText.split(/[ \n]/g);
  
  let result = '';
  let maxLengthReached = false;
  for (let word of words) {
    if (result.length + word.length + 3 > length) { maxLengthReached = true; break; }
    const whitespaceChar = optimizedText.charAt(result.length + word.length);
    result += word + whitespaceChar;
  }
  
  result = result.trim();
  
  if (/[?.!]$/g.test(result)) { return result; }
  
  result = result.replace(/[-–—,;]$/g, '');
  
  return maxLengthReached ? `${result} …` : result;
}

/**
 * Creates DOM Nodes from an HTML string.
 * 
 * @param {String} string - The html string, that shall be converted.
 * @return {DocumentFragment} 
 */
export function parseHtmlString(string) {
  return document.createRange().createContextualFragment(string);
}

/**
 * Creates a subset of `obj`, which only contains the given `keys`.
 * 
 * @param {Object}   obj - The original object
 * @param {String[]} keys - All list of keys, that will be included in the subset
 * @returns {Object} A subset of the original object with the given `keys`
 */
export function getObjectSubset(obj, keys) {
  return Object.entries(obj).reduce((newObj, [key, val]) => {
    return keys.includes(key) ? {...newObj, [key]: val } : newObj;
  }, {});
}