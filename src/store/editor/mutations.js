import Vue from 'vue';

const mutations = {
  addModule: (state, module) => {
    state.modules = [...state.modules, module];
  },
  
  setApi: (state, [moduleIndex, api]) => {
    Vue.set(state.modules[moduleIndex], 'api', api);
  },

  setActiveMarkupTags: (state, [moduleIndex, activeMarkupTags]) => {
    Vue.set(state.modules[moduleIndex], 'activeMarkupTags', activeMarkupTags);
  },
  
  setActiveSectionTags: (state, [moduleIndex, activeSectionTags]) => {
    Vue.set(state.modules[moduleIndex], 'activeSectionTags', activeSectionTags);
  },

  clearModules: (state) => {
    state.modules = [];
  },
};

export default mutations;