import MobiledocTextRenderer from 'mobiledoc-text-renderer';
import { shortenText } from '../../utils';

const EMPTY_POST_PLACEHOLDER_SIDEBAR = 'Empty post';

const getters = {
  posts: (state) => state.posts,

  postOrder: (state) => state.postOrder,

  getPostById: (state, { posts }) => (id) => posts[id],

  getOrderedPosts: (state, { getPostById, postOrder, posts }) => (order = 'date') => {
    switch (order) {
      case 'manual': 
        return postOrder.map(postId => getPostById(postId));
      case 'date':
        return Object.values(posts).sort((a, b) => b.metadata.created.getTime() - a.metadata.created.getTime());
    }
  },
  
  selectedPost: (state, { getPostById }) => getPostById(state.selectedPostId),

  selectedPostId: (state) => state.selectedPostId,
  
  selectedPostModules: (state, { selectedPost }) => selectedPost && selectedPost.modules || [],
  
  getSelectedPostModuleByIndex: (state, { selectedPostModules }) => (moduleIndex) => selectedPostModules[moduleIndex],
  
  selectedPostMeta: (state, { selectedPost }) => selectedPost && selectedPost.metadata || {},
  
  getSelectedPostMetaByKey: (state, { selectedPost }) => (key) => selectedPost && selectedPost.metadata && selectedPost.metadata[key],
  
  getPostTextById: (state, { getPostById }) => (id, asArray = false) => {
    const post = getPostById(id);
    // TODO: Add card and atom support
    const renderer = new MobiledocTextRenderer.default();
    const paragraphs = post.modules.map(module => renderer.render(module).result).join('\n').split('\n');
    return asArray ? paragraphs : paragraphs.join('\n');
  },

  getPostTitleById: (state, { generatePostTitleFromContent, getPostById }) => (id, maxLength = 0) => {
    const post = getPostById(id);
    if (post.metadata.title) { return post.metadata.title; }    
    return generatePostTitleFromContent(id, maxLength);
  },

  getSelectedPostTitle: (state, { selectedPostId, getPostTitleById }) => (maxLength = 0) => {
    return getPostTitleById(selectedPostId, maxLength);
  },

  generatePostTitleFromContent: (state, { getPostTextById }) => (id, maxLength = 0) => {
    const paragraphs = getPostTextById(id, true);
    if (paragraphs.length === 0 || paragraphs[0].length < 1) { return EMPTY_POST_PLACEHOLDER_SIDEBAR; }
    if (maxLength < 1 || paragraphs[0].length <= maxLength) { return paragraphs[0]; }
    return shortenText(paragraphs[0], maxLength);
  },

  generateSelectedPostTitleFromContent: (state, { generatePostTitleFromContent, selectedPostId }) => (maxLength = 0) => {
    return generatePostTitleFromContent(selectedPostId, maxLength);
  },

  getPostExcerptById: (state, { getPostTextById }) => (id, maxLength, excludeTitle = true) => {
    const paragraphs = getPostTextById(id, true);
    const textIsEmpty = paragraphs.length < 1
                     || paragraphs[0].length < 1
                     || (excludeTitle && paragraphs.length < 2)
                     || (excludeTitle && paragraphs[1].length < 2);
    
    if (textIsEmpty) { return ''; }

    const text = (excludeTitle ? paragraphs.slice(1) : paragraphs).join(' ');
    
    return shortenText(text, maxLength);
  },

  getPostsWithTags: (state, { posts }) => (tags) => {
    return posts
      .filter(post => tags.every(tag => post.metadata.tags && post.metadata.tags.includes(tag)));
  },
  
  selectedPostTemplate: (state, { selectedPost, getSelectedPostMetaByKey }, getters, rootGetters) => {
    if (!selectedPost) { return; }
    
    const {
      'project/getTemplateByName': getTemplateByName,
      'project/defaultTemplate': defaultTemplate,
    } = rootGetters;

    const templateName = getSelectedPostMetaByKey('template');
    return getTemplateByName(templateName) || defaultTemplate;
  },
};

export default getters;