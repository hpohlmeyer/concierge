import Vue from 'vue';
import Vuex from 'vuex';

import posts from './posts';
import editor from './editor';
import modals from './modals';
import project from './project';
import sidebar from './sidebar';
import blockMenu from './blockMenu';
import inlineMenu from './inlineMenu';

Vue.use(Vuex);

const store = new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    posts,
    editor,
    modals,
    project,
    sidebar,
    blockMenu,
    inlineMenu,
  },
});

// Enable hot module replacement for all store modules.
if (module.hot) {
  module.hot.accept([
    './posts',
    './editor',
    './modals',
    './project',
    './sidebar',
    './blockMenu',
    './inlineMenu',
  ], () => {
    store.hotUpdate({
      modules: {
        posts: require('./posts').default,
        editor: require('./editor').default,
        modals: require('./modals').default,
        project: require('./project').default,
        sidebar: require('./sidebar').default,
        blockMenu: require('./blockMenu').default,
        inlineMenu: require('./inlineMenu').default,
      },
    });
  });
}

export default store;