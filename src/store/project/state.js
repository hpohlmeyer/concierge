import cleanConfig from '../../utils/cleanConfig';

const state = {
  config: cleanConfig({}),
  projectPath: null,
};

export default state;