import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import 'vue-awesome/icons';

// The global event bus.
// It should be used exclusively for the communication
// with the MobiledocEditor, since its API can not 
// communicate with the store.
const EventBus = new Vue();
Vue.prototype.$editorApi = EventBus;

Vue.prototype.$log = console.log.bind(console);

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
});
