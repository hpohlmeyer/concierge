/**
 * Merges a config with some defaults, to ensure certain
 * properties are defined, even if they are not provided by the
 * config.
 * 
 * @param {Object} configuration - The configuration file, parsed as an object
 * 
 * @return {Object} The cleaned configuration object
 */
export default function (configuration) {
  // Clone the config
  const config = {};

  // Merge generator config with defaults
  config.generator = {
    buildCommand: undefined,
    ...configuration.generator,
  };

  // Merge editor config with defaults
  config.editor = {
    atoms: [],
    cards: [],
    markup: [],
    sections: [],
    ...configuration.editor,
  };

  // Add the _type property to the blocks
  config.editor.atoms = config.editor.atoms.map(atom => ({ ...atom, _type: 'atom' }));
  config.editor.cards = config.editor.cards.map(card => ({ ...card, _type: 'card' }));
  config.editor.markup = config.editor.markup.map(markup => ({ ...markup, _type: 'markup' }));
  config.editor.sections = config.editor.sections.map(section => ({ ...section, _type: 'section' }));

  // Merge ftp config with defaults
  config.ftp = {
    protocol: 'ftp',
    remotePath: '/',
    ...configuration.ftp,
  };

  // If the protocol is set, return the right default port
  config.ftp.port = config.ftp.port || (config.ftp.protocol === 'ftp' && 21 || config.ftp.protocol === 'sftp' && 22);

  // Merge the template config with defaults
  config.templates = {
    default: createDefaultTemplate(config.editor),
    ...configuration.templates,
  };

  config.templates = Object.entries(config.templates)
    .reduce((templates, [name, template]) => ({ ...templates, [name]: parseTemplate(template, config.editor) }), {});

  config.theme = Object.assign({}, configuration.theme);

  return config;
}

/**
 * Creates a default template for use in a config.
 * It consists of one ediotr, which supports all features
 * defined in the `editorConfig`.
 *
 * @param {Object} editorConfig The editor part of the projectConfig
 * @returns {Object} A template, that should be used as the default config
 *                   for a project config.
 */
function createDefaultTemplate(editorConfig) {
  return {
    modules: [
      { // Editor 1
        allowedAtoms: editorConfig.atoms.map(atom => atom.name),
        allowedCards: editorConfig.cards.map(card => card.name),
        allowedMarkup: editorConfig.markup.map(markup => markup.name),
        allowedSections: editorConfig.sections.map(sections => sections.name),
      },
    ],
  };
}

function parseTemplate(template, editorConfig) {
  const modules = template.modules.map(module => ({
    allowedAtoms: _parseAtoms(module, editorConfig),
    allowedCards: _parseCards(module, editorConfig),
    allowedMarkup: _parseMarkup(module, editorConfig),
    allowedSections: _parseSections(module, editorConfig),
  }));

  return { ...template, modules };
}

function _parseAtoms(module, editorConfig) {
  const atomsInConfig = editorConfig.atoms.map(atom => atom.name);
  return module.allowedAtoms
    .filter(atomName => atomsInConfig.includes(atomName))
    .map(atomName => editorConfig.atoms.find(atom => atom.name === atomName));
}

function _parseCards(module, editorConfig) {
  const cardsInConfig = editorConfig.cards.map(card => card.name);
  return module.allowedCards
    .filter(cardName => cardsInConfig.includes(cardName))
    .map(cardName => editorConfig.cards.find(card => card.name === cardName));
}

function _parseSections(module, editorConfig) {
  const sectionsInConfig = editorConfig.sections.map(section => section.name);
  return module.allowedSections
    .filter(sectionName => sectionsInConfig.includes(sectionName))
    .map(sectionName => editorConfig.sections.find(section => section.name === sectionName));
}

function _parseMarkup(module, editorConfig) {
  const markupInConfig = editorConfig.markup.map(markup => markup.name);
  return module.allowedMarkup
    .filter(markupName => markupInConfig.includes(markupName))
    .map(markupName => editorConfig.markup.find(markup => markup.name === markupName));
}