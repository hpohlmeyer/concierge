const actions = {
  create({ rootGetters, commit }) {
    const modules = rootGetters['posts/selectedPostModules'];
    modules
      .map(() => ({ api: null, activeMarkup: [], activeSections: [] }))
      .forEach(module => commit('addModule', module));
  },

  // Editor API Calls

  insertAtom({ getters }, [moduleIndex, name, text = '', payload = {}]) {
    const module = getters.getModule(moduleIndex);
    module.api.insertAtom(name, text, payload);
  },

  insertCard({ getters }, [moduleIndex, name, payload = {}, editMode = true]) {
    const module = getters.getModule(moduleIndex);
    module.api.insertCard(name, payload, editMode);
  },

  toggleMarkup({ getters }, [moduleIndex, tag]) {
    const module = getters.getModule(moduleIndex);
    module.api.toggleMarkup(tag);
  },

  toggleSection({ getters }, [moduleIndex, tag]) {
    const module = getters.getModule(moduleIndex);
    module.api.toggleSection(tag);
  },
  
  registerKeyCommand({ getters }, [moduleIndex, keyCommand]) {
    const module = getters.getModule(moduleIndex);
    module.api.registerKeyCommand(keyCommand);
  },

  reload({ getters }, [moduleIndex, keepCurrentContent = true]) {
    const module = getters.getModule(moduleIndex);
    module.api.reload(keepCurrentContent);
  },
  
  save({ getters }, moduleIndex) {
    const module = getters.getModule(moduleIndex);
    module.api.save();
  },
};

export default actions;