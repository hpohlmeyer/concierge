const {app, Menu, BrowserWindow} = require('electron');


const baseMenu = [
  {
    label: 'File',
    submenu: [
      {
        label: 'Open Project',
        id: 'open-project',
        accelerator: 'CmdOrCtrl+O',
        click() { BrowserWindow.getFocusedWindow().webContents.send('open-project'); },
      },
      {
        label: 'Close Project',
        id: 'close-project',
        accelerator: 'CmdOrCtrl+W',
        enabled: false,
        click() { BrowserWindow.getFocusedWindow().webContents.send('close-project'); },
      },
    ],
  },
  {
    label: 'Edit',
    submenu: [
      {role: 'undo'},
      {role: 'redo'},
      {type: 'separator'},
      {role: 'cut'},
      {role: 'copy'},
      {role: 'paste'},
      {role: 'delete'},
      {role: 'selectall'},
    ],
  },
  {
    label: 'Site',
    submenu: [
      {
        label: 'Preview',
        id: 'preview-site',
        enabled: false,
        click() { BrowserWindow.getFocusedWindow().webContents.send('preview-site'); },
      },
      {
        label: 'Export',
        id: 'export-site',
        enabled: false,
        click() { BrowserWindow.getFocusedWindow().webContents.send('export-site'); },
      },
      {
        label: 'Deploy',
        id: 'deploy-site',
        enabled: false,
        click() { BrowserWindow.getFocusedWindow().webContents.send('deploy-site'); },
      },
    ],
  },
  {
    label: 'View',
    submenu: [
      {role: 'reload'},
      {role: 'forcereload'},
      {role: 'toggledevtools'},
      {role: 'togglefullscreen'},
    ],
  },
  {
    role: 'window',
    submenu: [
      {role: 'minimize'},
      {role: 'close'},
    ],
  },
  {
    role: 'help',
    submenu: [
      {
        label: 'Learn More',
        click () { require('electron').shell.openExternal('https://electron.atom.io'); },
      },
    ],
  },
];

if (process.platform === 'darwin') {
  baseMenu.unshift({
    label: app.getName(),
    submenu: [
      {role: 'about'},
      {type: 'separator'},
      {role: 'services', submenu: []},
      {type: 'separator'},
      {role: 'hide'},
      {role: 'hideothers'},
      {role: 'unhide'},
      {type: 'separator'},
      {role: 'quit'},
    ],
  });

  // Edit menu
  baseMenu[2].submenu.push(
    {type: 'separator'},
    {
      label: 'Speech',
      submenu: [
        {role: 'startspeaking'},
        {role: 'stopspeaking'},
      ],
    }
  );

  // Window menu
  baseMenu[5].submenu = [
    {role: 'close'},
    {role: 'minimize'},
    {role: 'zoom'},
    {type: 'separator'},
    {role: 'front'},
  ];
}

module.exports = Menu.buildFromTemplate(baseMenu);
