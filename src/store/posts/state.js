const state = {
  searchQuery: '',
  selectedPostId: null,
  postOrder: [],
  posts: {},
};

export default state;