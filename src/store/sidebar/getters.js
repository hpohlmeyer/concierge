const getters = {
  filterTags: (state) => state.filter.tags,
  filterQuery: (state) => state.filter.query || '',
  filteredPosts: (state, { filterTags, filterQuery }, rootState, rootGetters) => {
    const {
      'posts/getOrderedPosts': getOrderedPosts,
      'posts/getPostTitleById': getPostTitleById,
    } = rootGetters;

    let filteredPosts = getOrderedPosts();
    // Filter by tags
    if (filterTags.length > 0) {
      filteredPosts = filteredPosts.filter(post => filterTags.every(tag => post.metadata.tags.includes(tag)));
    }

    // Filter by title
    if (filterQuery) {
      filteredPosts = filteredPosts.filter(post => {
        return getPostTitleById(post.id)
          .toLowerCase()
          .includes(filterQuery.toLowerCase());
      });
    }

    return filteredPosts;
  },
  isFiltered(state, { filterTags, filterQuery }) {
    return !!(filterTags.length || filterQuery);
  },
};

export default getters;