import { resolve } from 'path';

const getters = {
  availableAtoms: (state) =>  state.config && state.config.editor.atoms || [],
  
  availableCards: (state) => state.config && state.config.editor.cards || [],
  
  availableMarkup: (state) => state.config && state.config.editor.markup || [],
  
  availableSections: (state) => state.config && state.config.editor.sections || [],

  projectPath: (state) => state.projectPath,
  
  configPath: (state, { projectPath }) => projectPath ? resolve(projectPath, './config/index.mjs') : undefined,
  
  buildDir: (state, { projectPath }) => projectPath ? resolve(projectPath, './.build') : undefined,
  
  contentDir: (state, { projectPath }) => projectPath ? resolve(projectPath, './.content') : undefined,
  
  postsDir: (state, { contentDir }) => contentDir ? resolve(contentDir, './posts') : undefined,

  assetsDir: (state, { contentDir }) => contentDir ? resolve(contentDir, './assets') : undefined,

  generatorDir: (state, { projectPath }) => projectPath ? resolve(projectPath, './generator') : undefined,

  buildCommand: (state) => state.config && state.config.generator.buildCommand,

  getDefaultTemplate: (state) => state.config && state.config.templates.default || undefined,
  
  getTemplateByName: (state) => (name) => state.config && state.config.templates[name] || undefined,

  existingTags: (state, getters, rootState, { 'posts/posts': posts }) => {
    const tags = Object.values(posts).reduce((tags, post) => tags.concat(post.metadata.tags), []);
    return [...new Set(tags)];
  },

  theme: (state) => state.config && state.config.theme || {},

  availableTemplates: (state) => state.config && state.config.templates && Object.keys(state.config.templates) || [],

  ftpSettings: (state) => state.config && state.config.ftp,
};

export default getters;