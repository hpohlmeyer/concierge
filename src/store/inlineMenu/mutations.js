const mutations = {
  setPosition: (state, position) => state.position = position,
  setVisibility: (state, visibility) => state.visible = visibility,
  setOptions: (state, options) => state.options = options,
  setSelectedOption: (state, selectedOptions) => state.selectedOptions = selectedOptions,
};

export default mutations;