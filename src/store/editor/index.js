import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import state from './state';

const store = {
  namespaced: true,
  actions,
  mutations,
  getters,
  state,
};

export default store;