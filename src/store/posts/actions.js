import router from '../../router';
import fse from 'fs-extra';
import { remote } from 'electron';
import { resolve, basename, relative } from 'path';
import { createEmptyPost, recursiveReaddir, jsonDateReviver } from '../../utils';

const SAVE_DEBOUNCE_TIME = 3000;

let saveDebouncedTimers = {};

const actions = {
  async createNewPost({ commit, dispatch }, templateName) {
    const post = createEmptyPost(templateName);
    commit('addPost', post);
    await dispatch('savePostToDisk', post);
    await dispatch('selectPost', post.id);
  },

  async deletePost({ commit, getters, dispatch }, id) {
    const dialogOptions = {
      type: 'question',
      buttons: ['Yes', 'No'],
      title: 'Confirm',
      message: 'Do you realy want to delete this post? This can not be undone.',
    };

    const answer = remote.dialog.showMessageBox(remote.getCurrentWindow(), dialogOptions);
    if (answer === 1) { return; }
    
    // If the deleted post is currently selected, show empty post state
    if (getters.selectedPostId === id) { router.push('/post'); }
    
    commit('deletePost', id);
    await dispatch('deletePostFromDisk', id);
  },

  async selectPost({ commit, dispatch }, postId) {
    commit('editor/clearModules', null, { root: true });
    commit('selectPost', postId);
    await dispatch('editor/create', null, { root: true });
    router.push(`/post/${postId}`);
  },

  async updateCurrentPostModule({ getters, commit, dispatch }, [moduleId, content]) {
    const postId = getters.selectedPostId;
    commit('updatePostModule', [postId, moduleId, content]);
    await dispatch('savePostToDiskDebounced', getters.selectedPost);
  },
  
  async updateCurrentPostMeta({ getters, commit, dispatch }, [metaKey, metaVal]) {
    const postId = getters.selectedPostId;
    commit('updatePostMeta', [postId, metaKey, metaVal]);
    await dispatch('savePostToDiskDebounced', getters.selectedPost);
  },

  async savePostToDisk({ rootGetters }, post) {
    const postPath = resolve(rootGetters['project/postsDir'], `post${post.id}.json`);
    const content = JSON.stringify(post);
    await fse.writeFile(postPath, content);
  },

  async savePostToDiskDebounced({ dispatch }, post) {
    const id = post.id;
    if (saveDebouncedTimers[id]) { clearTimeout(saveDebouncedTimers[id]); }
    saveDebouncedTimers[id] = setTimeout(() => {
      dispatch('savePostToDisk', post);
      delete saveDebouncedTimers[id];
    }, SAVE_DEBOUNCE_TIME);
  },
  
  async loadPostFromDisk({ commit }, path) {
    try {
      const post = await fse.readFile(path, 'utf8');
      // TODO: Validate the post object.
      //       If it has been modified outside, it may be broken,
      //       which in turns breaks the whole code. Offer to delete it!
      commit('addPost', JSON.parse(post, jsonDateReviver));
    } catch (err) {
      console.error(err);
    }
  },

  async deletePostFromDisk({ rootGetters }, id) {
    if (saveDebouncedTimers[id]) { clearTimeout(saveDebouncedTimers[id]); }
    delete saveDebouncedTimers[id];

    // Delete assets
    const postAssetsDir = resolve(rootGetters['project/assetsDir'], id);
    await fse.remove(postAssetsDir);

    // Delete the post
    const filename = `post${id}.json`;
    const postPath = resolve(rootGetters['project/postsDir'], filename);
    await fse.remove(postPath);
  },

  setPostFilter({ commit }, query) {
    commit('setSearchQuery', query);
  },

  clearPostFilter({ commit }) {
    commit('setSearchQuery', '');
  },

  async saveAsset({ getters, rootGetters }, { filePath, subPath = '' }) {
    const {
      'project/assetsDir': assetsDir,
      'project/contentDir': contentDir,
    } = rootGetters;
    const filename = basename(filePath);
    const fullPath = resolve(assetsDir, getters.selectedPostId, subPath, filename);
    const relativePath = relative('.', fullPath);
    const renderPath = relative(contentDir, fullPath);

    const pathExists = await fse.pathExists(fullPath);
    if (pathExists) {
      const dialogOptions = {
        type: 'question',
        buttons: ['Yes', 'No'],
        title: 'Confirm',
        message: 'A file with this name already exists. Do you want to replace it?',
      };
      const choice = remote.dialog.showMessageBox(remote.getCurrentWindow(), dialogOptions);
      if (choice === 1) { throw new Error('Overwrite canceled due to user input.'); }
    }

    await fse.copy(filePath, fullPath);

    return {
      fullPath,
      relativePath,
      renderPath, // Relative to the root build path
    };
  },

  async deleteAsset(ctx, { assetPath, force = false }) {
    if (!fse.pathExists(assetPath)) { return; }
    if (!force) {
      const choice = remote.dialog.showMessageBox(remote.getCurrentWindow(), {
        type: 'question',
        buttons: ['Yes', 'No'],
        title: 'Confirm',
        message: 'Delete the file?',
      });
      if (choice === 1) { throw new Error('File deletion canceled by the user.'); }
    }
    await fse.remove(assetPath);
    // TODO: Delete empty folders.
  },

  async listAssets({ getters, rootGetters }, [subPath = '']) {
    try {
      const { 'project/assetsDir': assetsDir } = rootGetters;
      const directory = resolve(assetsDir, getters.selectedPostId, subPath);
      return await recursiveReaddir(directory);
    } catch(err) {
      return [];
    }
  },
};

export default actions;